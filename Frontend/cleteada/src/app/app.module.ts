import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

//To Show message to the user
import {ToastrModule} from 'ngx-toastr';

//To connection http
import { UploadService } from './services/upload.service';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavegationComponent } from './navegation/navegation.component';
import { ContentMainComponent } from './content-main/content-main.component';
import { ReportsComponent } from './reports/reports.component';
import { HistoricComponent } from './historic/historic.component';

//Datepicker with ngx-bootstrap
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

//Slider
import {SlideshowModule} from 'ng-simple-slideshow';

/*
https://www.tutorialspoint.com/angular6/angular6_routing.htm
https://academia-binaria.com/paginas-y-rutas-angular-spa/
http://developinginspanish.com/2018/03/13/tutorial-angular-7-enrutamiento/
https://codecraft.tv/courses/angular/routing/navigation/
*/

const routes: Routes = [
  {path: '' , component: ContentMainComponent}, //content of start
  {path: 'navegation' , component: NavegationComponent},
  {path: 'reports' , component: ReportsComponent},
  {path: 'historic' , component: HistoricComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavegationComponent,
    ContentMainComponent,
    ReportsComponent,
    HistoricComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 20000,      // how many seconds do you want the message to be displayed?
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    SlideshowModule,
  ],
  providers: [UploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
