import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-content-main',
  templateUrl: './content-main.component.html',
  styleUrls: ['./content-main.component.css']
})
export class ContentMainComponent implements OnInit {
  public imagesUrl;
  constructor() { }

  ngOnInit() {
    this.imagesUrl = ['assets/slide1.jpg','assets/slide2.jpg','assets/slide3.jpg'];
  }

}
