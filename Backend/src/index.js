import app from  './app';

//Constants
const PORT = 3000;   //port of connection

function main() {
    app.listen(PORT, () => console.log(`Server listening on PORT ${PORT}!`))
}

main();