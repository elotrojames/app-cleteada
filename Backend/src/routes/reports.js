import {Router} from 'express';
const router = Router();

//from reports.controller
import {createReport, getReports, getOneReport, deleteReport, updateReport, 
    getReportByDate, getReportByDateAndType} from '../controllers/reports.controller';

// http/localhost:3000/api/Reports/  --> This is the way to access these routes.
router.post('/', createReport);
router.get('/', getReports);

// http/localhost:3000/api/Reports/accidente or robo
router.get('/:startDate/:endDate', getReportByDate);
router.get('/:type', getOneReport);
router.get('/:startDate/:endDate/:type', getReportByDateAndType);

router.delete('/:id', deleteReport);
router.put('/:id', updateReport);

export default router;