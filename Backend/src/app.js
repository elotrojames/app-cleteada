import express, {json} from 'express';
import morgan from 'morgan'; // To see for console the requests
import cors from 'cors'; //petitions 
import path from 'path'; //To path join
import fs from 'fs'; //To rename file
import bodyParser from 'body-parser'

//Importing routes
import reportRoutes from './routes/reports';
import {uploadImage, uploadGpx} from './upload';
import {testConnection, createGpxfile, getFiles} from './controllers/gpxfiles.controller.js';

//Initializations
const app = express();


//Middlewares
app.use(json()); //This to when sometype of aplication send a json file, the server can read it
app.use(morgan('dev')); //To show logs by console/terminal
app.use(cors()); //Cors to petitions http, https://developer.mozilla.org/es/docs/Web/HTTP/Access_control_CORS
app.use(bodyParser.urlencoded({
    extended: true
}));

//Routes -> These routes can to be use by the aplication
//The routes must always be at the end
app.use('/api/reports',reportRoutes);

//Testing db connection
//testConnection()

//To save the upload file with its original name
app.post('/api/upload-image', uploadImage, (req, res, next) =>{ 
    var fileName = req.files.image.name; //the file name for the DB register 

    var new_path = path.join(__dirname,'../uploads/images', fileName);
    var old_path = req.files.image.path;

    fs.rename(old_path, new_path , function (err) {
        if (err) {
            console.log(err);
        }
    });
    res.status(200).json({
        'message': 'File uplouded succefully.',
        'var': fileName
    });
});

app.post('/api/upload-gpx', uploadGpx, (req, res, next) =>{
    var fileName = req.files.file.name; //the file name for the DB register 

    var new_path = path.join(__dirname,'../uploads/gpx-files', fileName);
    var old_path = req.files.file.path;

    // Catch a bad extension for the file 
    var array = fileName.split('.'); //to get the extension 
    var extension = array[(array.length) - 1] //get the last element of the array
    
    if (extension == 'gpx'){
        // console.log('Correct extension')

        try {
            fs.rename(old_path, new_path , function (err) { //rename file with original name
                if (err) {
                    console.log(err,'hola');
                }
            });
        
            //Create a db insertion with the file info
            //createGpxfile(fileName, new_path) 
    
            res.status(200).json({
                'message': 'File uplouded succefully.',
                'var': fileName
            });
    
        } catch (error){

            console.error('Error during the request ', error);
            // 400 Bad Request
            res.status(400).json({
                'message': 'Can\'t uploud the file.',
                'var': fileName 
            });
        }

    } else {
        // 415 Unsupported Media Type
        console.log('Bad extension from the file in upload-gpx')
        res.status(415).json({
            'message': 'File extension not supported.',
            'var': fileName
        });
    }
});

// Route to request the best routes  
app.post('/api/request-routes', (req, res,  next) => {
    var name = req.body.nombre 
    var sunny = req.body.soleado 
    var trafic = req.body.trafico
    var hills = req.body.pendientes
    

    // Call the getfiles function with 
    //var results = await getFiles(name, sunny, trafic, hills)

    // console.log(results.length)
    console.log(results)

    // Use res.download or the other one to send the files 

    // console.log(req) 
    // console.log(req.body) 

    // res.json({
    //     'recibido' : '1',
    //     'nombre' : name,
    //     'sol' : sunny,
    //     'trafico' : trafic,
    //     'pendiente' : hills
    // });
    
});

app.get('/api/download-test', (req, res, next) => {
    res.download('uploads/gpx/thegpx.gpx');
});

export default app;