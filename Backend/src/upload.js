import multipart from 'connect-multiparty'; 

//Middlewares
export const uploadImage = multipart({
    uploadDir: './uploads/images'
});

export const uploadGpx = multipart({
    uploadDir: './uploads/gpx-files'
});



