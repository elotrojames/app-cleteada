# app-cleteada

# Android

# Frontend
Para ejecutar el programa debe tener instalado angular CLI y usar ng serve -o

# Backend
Para levantar el servidor debe usar el siguiente  comando "npm run dev-start" este levantara con el servidor index.js. 
Puede ver dev-start en el archivo package.json ahi puede configurar el archivo de arranque por defecto esta src/index.js. Si desea levantar servidor.js use nodemon server.js o cambie dev-start.

# Directorios de almacenamiento
Los archivos que se suben al servidor se guardan en el directorio "uploads"

# Insomnia o  Postman
Para realizar consultas al servidor puede usar Insomnia este programa permite realizar consultas de una forma muy sencilla. Puede descargarlo aqui https://insomnia.rest/download/ o puede utilizar Postman también es sencillo de usar.